import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { Layout } from "./components";
import { FlightsPage } from "./pages";

export function AppRoutes() {
  return (
    <Router>
      <Layout>
        <Routes>
          <Route path="/flights" element={<FlightsPage />}></Route>
        </Routes>
      </Layout>
    </Router>
  );
}
