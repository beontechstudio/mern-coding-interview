import axios, { AxiosInstance, AxiosResponse } from "axios";
import Environment from "../environment";
import { FlightModel } from "../models/flight.model";

const base = Environment.backendApi;

export class BackendClient {
  protected axiosInstance: AxiosInstance;
  protected constructor(baseUrl: string) {
    this.axiosInstance = axios.create({
      baseURL: baseUrl,
    });
  }

  private static _instance: BackendClient;
  static getInstance(url: string = base) {
    if (!BackendClient._instance) {
      BackendClient._instance = new BackendClient(url);
    }
    return BackendClient._instance;
  }

  protected _handleResponse<T = any>(axiosResponse: AxiosResponse<T>) {
    return axiosResponse.data;
  }

  getFlights = () => {
    return this.axiosInstance
      .get<FlightModel[]>(`/flights`)
      .then(this._handleResponse);
  };
}
