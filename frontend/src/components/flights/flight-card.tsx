import React from "react";
import { FlightStatuses } from "../../models";

interface FlightCardProps {
  code: string;
  origin: string;
  destination: string;
  passengers?: string[];
  status: FlightStatuses;
}

const mapFlightStatusToColor = (status: FlightStatuses) => {
  const mappings = {
    [FlightStatuses.Arrived]: "#1ac400",
    [FlightStatuses.Delayed]: "##c45800",
    [FlightStatuses["On Time"]]: "#1ac400",
    [FlightStatuses.Landing]: "#1ac400",
    [FlightStatuses.Cancelled]: "#ff2600",
  };

  return mappings[status] || "#000000";
};

export const FlightCard: React.FC<FlightCardProps> = (
  props: FlightCardProps
) => (
  <div className="card">
    <div style={{ display: "flex", justifyContent: "space-between" }}>
      <strong>{props.code}</strong>

      <span style={{ color: mapFlightStatusToColor(props.status) }}>
        Status: {props.status}
      </span>
    </div>

    <span>Origin: {props.origin}</span>
    <br />
    <span>Destination: {props.destination}</span>
  </div>
);
